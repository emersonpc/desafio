<?php 
session_start();
include('includes/conexao.php');
include('includes/cabecalho.php');
?>

<body>
  <!-- Main Content -->
  <main class="content">

    <?php 
    if (!empty($_SESSION['mensagem'])) {
      echo '<h3>'.$_SESSION['mensagem'].'</h3>';
      unset($_SESSION['mensagem']);
    }
    ?>

    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="addProduct.php" class="btn-action">Add new Product</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>

      <?php
      $produtos = $conn->query('SELECT * FROM products');

      foreach ($produtos as $produto) { 
        $categoriasProduto = $conn->query('SELECT C.* FROM products_categories PC INNER JOIN categories C ON PC.category_id = C.id INNER JOIN products PR ON PC.product_id = PR.id WHERE PC.product_id = '.$produto['id']);
      ?>
      
        <tr class="data-row">
          <td class="data-grid-td">
             <span class="data-grid-cell-content"><?=$produto['name']?></span>
          </td>
        
          <td class="data-grid-td">
             <span class="data-grid-cell-content"><?=$produto['sku']?></span>
          </td>

          <td class="data-grid-td">
             <span class="data-grid-cell-content">R$ <?=number_format($produto['price'], 2, ',', '.')?></span>
          </td>

          <td class="data-grid-td">
             <span class="data-grid-cell-content"><?=$produto['quantity']?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content">
              <?php foreach ($categoriasProduto as $categoriaProduto) {            
                echo $categoriaProduto['name'].'<br/>';
              } ?>
            </span>
          </td>
        
          <td class="data-grid-td">
            <div class="actions">
              <div class="action edit"><span>Edit</span></div>
              <div class="action delete"><span>Delete</span></div>
            </div>
          </td>
        </tr>


      <?php
      }
      ?>
    </table>
  </main>
  <!-- Main Content -->

<?php 
include('includes/footer.php');
?>