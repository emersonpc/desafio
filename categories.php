<?php 
session_start();
include('includes/conexao.php');
include('includes/cabecalho.php');
?>
<body>
  <!-- Main Content -->
  <main class="content">
    
    <?php 
    if (!empty($_SESSION['mensagem'])) {
      echo '<h3>'.$_SESSION['mensagem'].'</h3>';
      unset($_SESSION['mensagem']);
    }
    ?>

    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="addCategory.php" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>

      <?php
      $categorias = $conn->query('SELECT * FROM categories');
      foreach ($categorias as $categoria) { 
      ?>

      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$categoria['name']?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$categoria['code']?></span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit"><span>Edit</span></div>
            <div class="action delete"><span>Delete</span></div>
          </div>
        </td>
      </tr>

      <?php
      }
      ?>      

    </table>
  </main>
  <!-- Main Content -->

<?php 
include('includes/footer.php');
?>