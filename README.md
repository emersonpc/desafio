**Desafio backend Web Jump**

Segue no repositório o arquivo loja_desafio_webjump.sql para utilizar no projeto desafio.
Local do arquivo de conexão com banco de dados mysql: includes/conexao.php

---

## Rodar o projeto

Faça um git clone do projeto em sua máquina e execute com algum server xampp/wamp ou outro.


## Você vai encontrar

1. Cadastro de produtos
2. Cadastro de categorias
3. Lista de produtos
4. Lista de categorias
5. Geração logs de ações

---