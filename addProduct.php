﻿<?php
session_start();
include('includes/conexao.php');
include('includes/funcoes.php');

if (!empty($_POST)) {
	$insereProduto = $conn->query('INSERT INTO products (sku, name, price, quantity, description, date_created) VALUES ("'.$_POST['sku'].'", "'.$_POST['name'].'", "'.$_POST['price'].'", "'.$_POST['quantity'].'", "'.$_POST['description'].'", "'.date("Ymd").'")');

	$produtoId = $conn->lastInsertId();
	if (!empty($_POST['category'])) {
		foreach ($_POST['category'] as $category) {
			$insereCategoria = $conn->query('INSERT INTO products_categories (product_id, category_id, date_created) VALUES ("'.$produtoId.'", "'.$category.'", "'.date("Ymd").'")');
		}
	}

	if ($insereProduto) {
		adiciona_log('Produto <strong>'.$_POST['name'].'</strong> cadastrado');
		$_SESSION["mensagem"] = 'Produto '.$_POST['name'].' inserido com sucesso.';
		header('Location: products.php');
		exit;
	} else {
		$_SESSION["mensagem"] = 'Houve um erro ao gravar o produto. Tente novamente!';
		exit;
	}
}
?>

<?php
include('includes/cabecalho.php');
?>

  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Product</h1>
    
    <form action="addProduct.php" method="POST">
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name="name" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="price" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" name="quantity" class="input-text" /> 
      </div>

      <?php
      $categorias = $conn->query('SELECT * FROM categories');
      ?>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category" name="category[]" class="input-text">
        	<?php foreach ($categorias as $categoria) { ?>
        		<option value="<?=$categoria['id']?>"><?=$categoria['name']?></option>
        	<?php } ?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text"></textarea>
      </div>
      <div class="actions-form">
        <a href="products.php" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->

<?php 
include('includes/footer.php');
?>