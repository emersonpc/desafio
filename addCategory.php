<?php
session_start();
include('includes/conexao.php');
include('includes/funcoes.php');

if (!empty($_POST)) {
	$insereCategoria = $conn->query('INSERT INTO categories (name, code, date_created) VALUES ("'.$_POST['name'].'", "'.$_POST['code'].'", "'.date("Ymd").'")');

	if ($insereCategoria) {
		adiciona_log('Categoria <strong>'.$_POST['name'].'</strong> cadastrada');
		$_SESSION["mensagem"] = 'Categoria '.$_POST['name'].' inserido com sucesso.';
		header('Location: categories.php');
		exit;
	} else {
		$_SESSION["mensagem"] = 'Houve um erro ao gravar o produto. Tente novamente!';
		exit;
	}
}
?>

<?php
include('includes/cabecalho.php');
?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Category</h1>
    
    <form action="addCategory.php" method="POST">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" name="name" class="input-text" />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" id="category-code" name="code" class="input-text" />
        
      </div>
      <div class="actions-form">
        <a href="categories.php" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save" />
      </div>
    </form>
  </main>
  <!-- Main Content -->

<?php 
include('includes/footer.php');
?>