<?php 
session_start();
include('includes/conexao.php');
include('includes/cabecalho.php');
?>

<body>
  <!-- Main Content -->
  <main class="content">
    
    <div class="header-list-page">
      <h1 class="title">Logs</h1>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Action</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Date</span>
        </th>        
      </tr>

      <?php
      $logs = $conn->query('SELECT * FROM logs');
      foreach ($logs as $log) { 
      ?>

      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$log['action']?></span>
        </td>
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$log['date_created']?></span>
        </td>        
      </tr>

      <?php
      }
      ?>      

    </table>
  </main>
  <!-- Main Content -->

<?php 
include('includes/footer.php');
?>