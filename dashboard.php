<?php 
session_start();
include('includes/conexao.php');
include('includes/cabecalho.php');
?>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
      <?php 
        $countProducts = $conn->query('SELECT count(id) as total FROM products');
        $countProducts->execute();
        $qtd = $countProducts->fetch(PDO::FETCH_ASSOC);
      ?>
      You have <?=$qtd['total']?> products added on this store: <a href="addProduct.php" class="btn-action">Add new Product</a>
    </div>

    <?php
      $produtos = $conn->query('SELECT * FROM products');
      if ($qtd['total'] > 0) { 
    ?>
        <ul class="product-list">
          <?php
            foreach ($produtos as $produto) { 
          ?>
            <li>
              <div class="product-image">
                <img src="images/product/no-foto.png" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
              </div>
              <div class="product-info">
                <div class="product-name"><span><?=$produto['name']?></span></div>
                <div class="product-price"><span class="special-price">9 available</span> <span>R$ <?=number_format($produto['price'], 2, ',', '.')?></span></div>
              </div>
            </li>
          <?php
            }
          ?>
        </ul>
      <?php
        } else {
      ?>    
      <h3 class="text-center">No registered products</h3>
    <?php } ?>
  </main>
  <!-- Main Content -->

<?php 
include('includes/footer.php');
?>